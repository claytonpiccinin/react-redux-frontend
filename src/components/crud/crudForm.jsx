import React, { Component } from 'react';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import {Grid} from '../helper/bootstrapHelper'
import IconButton from '../helper/buttonHelper'
import { add, changeDescription, search, clear } from '../../actions/crudActions'

class crudForm extends Component{
    constructor(props){
        super(props)
        this._keyHandler = this._keyHandler.bind(this)
    }

    componentWillMount(){
        this.props.search()
    }

    _keyHandler (e){
        const { clear, add, search, description } = this.props
        console.log(e.key + ' ' + e.shiftKey)
        if(e.key === 'Enter') {
            e.shiftKey ? search() : add(description)
        } else if (e.key === 'Escape') {
            clear()
        } 
    }

    render(){
        const { add, clear, description, search } = this.props
        return(
            
            <div role='form' className='crudForm'>
                <Grid cols='12 9 10'>
                    <input id='description' 
                        className='form-control' 
                        placeholder='Adicione uma tarefa'
                        onKeyUp={this._keyHandler}
                        value={this.props.description}
                        onChange={this.props.changeDescription}/>
                </Grid>
                <div className='12 3 2'>
                    <IconButton style='primary' icon='plus' onClick={() => add(description)}/>
                    <IconButton style='info' icon='search' onClick={search}/>
                    <IconButton style='default' icon='close' onClick={clear}/>
                </div>
            </div>
        )
    }


}

const mapStateToProps = state => ({ description : state.crud.description })
const mapDispatchToProps = dispatch => bindActionCreators({ clear, add, changeDescription, search }, dispatch)
export default connect(mapStateToProps, mapDispatchToProps)(crudForm)