import React from 'react'
import ReactDOM from 'react-dom'
import { applyMiddleware, createStore } from 'redux';
import { Provider } from 'react-redux'

// Middlewares
import promisse from 'redux-promise'
import multi from 'redux-multi'
import thunk from 'redux-thunk'

// APP
import App from  './components/main/app'
import { rootReducer } from './store/stores'

// Configuração do Redux DevTools 
const devTools = window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()

const store = applyMiddleware(thunk, // Permite o uso do dispatch na Action
                              multi, // Disparar um array de actions
                              promisse // Sempre que disparar uma promisse na Action, ele espera a promisse ser resolvida para então gerar a Action e disparar o reducer
                            )(createStore)(rootReducer, devTools) // Passar os middlewares para CreateStore e então passar tudo para o Reducer e devTools

ReactDOM.render( 
    <Provider store={store}>
        <App />
    </Provider>
, document.getElementById('app'))