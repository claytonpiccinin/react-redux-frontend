import React from 'react';
import { connect } from 'react-redux'
import IconButton from '../helper/buttonHelper'
import { bindActionCreators } from 'redux'
import { markAsDone, markAsUndone, remove } from '../../actions/crudActions'

import '../../../public/assets/custom.css'

const crudList = props => {

    const renderRows = () => {
        const list = props.list || []

        return(
            list.map(crud => (
                <tr key={crud._id}>
                    <td className={crud.done ? 'markAsDone' : ''}>{crud.description}</td>
                    <td>
                    <IconButton style='success' icon='check' hide={crud.done} onClick={() => props.markAsDone(crud)} />
                    <IconButton style='danger'  icon='trash-o' hide={!crud.done} onClick={() => props.remove(crud)}/>
                    <IconButton style='warning' icon='undo' hide={!crud.done} onClick={() => props.markAsUndone(crud)} />
                    </td>
                </tr>
            ))
        )
    }
    return (
        <table className="table">
            <thead>
                <tr>
                    <th>Descrição</th>
                    <th className="tableActions">Ações</th>
                </tr>
            </thead>
            <tbody>
                {renderRows()}
            </tbody>
        </table>
    )
}

const mapStateToProps = state => ({list: state.crud.list})
const mapDispatchToProps = dispatch => bindActionCreators({markAsDone, markAsUndone, remove}, dispatch)
export default connect( mapStateToProps, mapDispatchToProps )(crudList)
