import React from 'react';

import PageHeader from '../menu/pageHeader'
import CrudForm from './crudForm'
import CrudList from './crudList'


export default props => (
            <div>
                <PageHeader name="Cadastro de tarefas" small="Star Wars Project" />
                <CrudForm />
                <CrudList />
            </div>
        )
