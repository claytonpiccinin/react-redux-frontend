import { combineReducers } from 'redux'
import  crudReducer from '../reducers/crudReducer'

export const rootReducer = combineReducers({
    crud: crudReducer
})
