import 'modules/bootstrap/dist/css/bootstrap.min.css' // Import bootstrap
import 'modules/font-awesome/css/font-awesome.min.css' // Import Font-Awesome
import React from 'react' // Import react
import Menu from '../menu/menu'
import Routes from '../main/routes'

export default props => (
    <div className="container">
        <Menu />
        <Routes />
    </div>
)