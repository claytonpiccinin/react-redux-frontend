import React from 'react';
import { Router, Route, Redirect, hashHistory } from  'react-router'

import Crud from '../crud/crud'
import About from '../about/about'

export default props => (
    <Router history={hashHistory}>
        <Route path='/crud' component={Crud} />
        <Route path='/about' component={About} />
        <Redirect from='*' to='Crud' />
    </Router>
)