export default( state = {}, action ) => {
    switch (action.type) {
        case 'DESCRIPTION_CHANGED': return {...state, description : action.payload }
        case 'CRUD_SEARCHED' : return {...state, list : action.payload}
        case 'CRUD_CLEARED' : return {...state, description: ''}
        case 'CRUD_MARK_AS_DONE' : return {...state, list: action.payload.data}
        case 'CRUD_MARK_AS_UNDONE' : return {...state, list : action.payload.data}
        case 'CRUD_REMOVE' : return { ...state, list: action.payload.data }
    }
    return state
}