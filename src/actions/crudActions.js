// Actions 
import axios from 'axios'

const URL = 'http://localhost:3000/api/crud/' // URL Backend API

export const changeDescription = event => ({
    type : 'DESCRIPTION_CHANGED',
    payload : event.target.value
})

export const search = () => {
    
    return(dispatch, getState) => {
        console.log(getState())
        let description = getState().crud.description
        let search = description ? `&description__regex=/${description}/i` : ''
        let request = axios.get(`${URL}?sort=-createdAt${search}`) // Async request will never return request.data before call reducer. 
                           .then(resp => dispatch ({ type : 'CRUD_SEARCHED', payload: resp.data}))
    }
}

export const add = description => {
    return dispatch => { axios.post(URL, { description })
                              .then(resp => dispatch(clear()))
                              .then(resp => dispatch(search()))
    }
}
export const clear = () => {
    return [{ type : 'CRUD_CLEARED'}, search()]
}

export const markAsDone = crud => {
    return dispatch => { axios.put(`${URL}${crud._id}`, {...crud, done: true})
                              .then(resp => dispatch({ type: 'CRUD_MARK_AS_DONE', payload : resp.data}))
                              .then(resp => dispatch(search()))
    }
}

export const markAsUndone = crud => {
    return dispatch => { axios.put(`${URL}${crud._id}`, {...crud, done: false})
                              .then(resp => dispatch({ type: 'CRUD_MARK_AS_UNDONE', payload : resp.data}))
                              .then(resp => dispatch(search()))
    }
}

export const remove = crud => {
    return dispatch => { axios.delete(`${URL}${crud._id}`)
                              .then(resp => dispatch({ type: 'CRUD_REMOVE', payload : resp.data}))
                              .then(resp => dispatch(search()))
    }
}
